// Note: This example requires that you consent to location sharing when
// prompted by your browser. If you see the error "The Geolocation service
// failed.", it means you probably did not give permission for the browser to
// locate you.
let map, infoWindow;

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: -34.397, lng: 150.644 },
    zoom: 6,
  });
  infoWindow = new google.maps.InfoWindow();

  const locationButton = document.createElement("button");
  const locationSwitch = document.createElement("input");
  locationSwitch.type = 'checkbox';
  locationSwitch.setAttribute('id','arfrsh');

  locationButton.textContent = "Pan to Current Location";
  locationSwitch.textContent = "Autorefresh";
  locationButton.classList.add("custom-map-control-button");
  locationSwitch.classList.add("custom-map-control-button");
  map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationButton);
  map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationSwitch);


  	locationButton.addEventListener("click", getLocation);
  	locationSwitch.addEventListener("change", checkForAutoRefresh);

}
var int = null;
const checkForAutoRefresh = (e) => {
  //  console.log(document.readyState);
 //   console.log(e);

    if (e.target.checked){
     //   console.log(true);
    //    console.log(int);
            int = setInterval(locationLoop, 5000);
     // int();
    //   console.log(int);
    }else{
     //   console.log(false);
   //     console.log('unchecked');
    //    console.log(int);
        clearInterval(int);
    //    console.log(int);
    }
  //  console.log("ggg");

}

async function locationLoop(){
    console.log('vvv');
    await locationGetter();
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(
    browserHasGeolocation
      ? "Error: The Geolocation service failed."
      : "Error: Your browser doesn't support geolocation."
  );
  infoWindow.open(map);
}


  const getLocation = (e) => {
        e.preventDefault();
      
        const formData = JSON.stringify({
                    "user_id": 1
                    //,"password" : values.password
                 });
      
         return fetch(window.location.protocol + '//' + window.location.host + window.location.pathname + '/postData', {
             method: 'POST',
             body: JSON.stringify(formData),
             headers: {
                 'Content-Type': 'application/json'
             }
         }).then(response => {
             if (response.status >= 200 && response.status < 300) {
              //  localStorage.setItem('location', values.location)
              //  localStorage.setItem('accuracy', values.accuracy)
                console.log("aaa");
                console.log(response);
                console.log("bbb");
               // setLoggedin(true);
               // window.location= '/';
                return response;
            } else {
              //  setLoggedin(false);
               handleLocationError(false, infoWindow, map.getCenter());
                console.log('Something happened wrong');
               }
         }).then(res => res.json()).then(data => {
                console.log("zzz");
                console.log(data);
                const pos = {
                            lat: data.latitude,
                            lng: data.longitude,
                          };

                          infoWindow.setPosition(pos);
                          infoWindow.setContent("Location found.");
                          infoWindow.open(map);
                          map.setCenter(pos);
         }).catch(err => err);
          }

function locationGetter(){
    //    e.preventDefault();

        const formData = JSON.stringify({
                    "user_id": 1
                    //,"password" : values.password
                 });

         return fetch(window.location.protocol + '//' + window.location.host + window.location.pathname + '/postData', {
             method: 'POST',
             body: JSON.stringify(formData),
             headers: {
                 'Content-Type': 'application/json'
             }
         }).then(response => {
             if (response.status >= 200 && response.status < 300) {
              //  localStorage.setItem('location', values.location)
              //  localStorage.setItem('accuracy', values.accuracy)
             //   console.log("aaa");
             //   console.log(response);
             //   console.log("bbb");
               // setLoggedin(true);
               // window.location= '/';
                return response;
            } else {
              //  setLoggedin(false);
               handleLocationError(false, infoWindow, map.getCenter());
                console.log('Something happened wrong');
               }
         }).then(res => res.json()).then(data => {
           //     console.log("zzz");
             //   console.log(data);
                const pos = {
                            lat: data.latitude,
                            lng: data.longitude,
                          };

                          infoWindow.setPosition(pos);
                          infoWindow.setContent("Location found.");
                          infoWindow.open(map);
                          map.setCenter(pos);
         }).catch(err => err);
          }