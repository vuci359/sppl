package hr.fer.SPPL.rest;

import hr.fer.SPPL.model.Location;
import hr.fer.SPPL.repo.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.MimeType;
import org.springframework.http.MediaType;
import java.net.URI;
import java.util.List;
import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.json.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.exc.StreamReadException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.ObjectCodec;

@RestController
@RequestMapping("")
public class LocationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocationController.class);


    @Autowired
    private LocationRepository locationRepository;

    @RequestMapping(value = "/postData",
            produces = "application/json",
            method = {RequestMethod.PUT, RequestMethod.POST, RequestMethod.GET})
    public ResponseEntity<Location> postData(@RequestBody JSONObject node) {
           int user_id = (Integer) node.get("user_id");
           System.out.println("user_id: "+user_id);
        Location location = locationRepository.findFirstByUserIdOrderByLocationIdDesc(user_id); //hard kodirano za potrebe testiranja
        System.out.println("lokacija: "+location);
        System.out.println("sve lokacije: "+locationRepository.findAll());


        if (location != null) {
            System.out.println(location);
            return new ResponseEntity<>(location, HttpStatus.OK);
        } else {
            System.out.println("nema lokacije");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping(value = "/getData", method = {RequestMethod.POST, RequestMethod.PUT, RequestMethod.GET},

            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<Location> getLocation(@RequestBody JSONObject node) {
        System.out.println(node);
        ObjectMapper objectMapper = new ObjectMapper();

        System.out.println(node);
        System.out.println(node.getJSONObject("location").get("lat").getClass().getName());

        Location location = new Location(
                1, //int user_id
                (double) ((Number)node.getJSONObject("location").get("lat")).doubleValue(), //double latitude
                (double) ((Number)node.getJSONObject("location").get("lng")).doubleValue(), //double longitude
                (double) ((Number)node.get("accuracy")).doubleValue() //long accuracy
        );

        System.out.println(location.toString());

        Location loc = locationRepository
                .save(location);

        if (loc != null) {
            System.out.println(loc);
            return new ResponseEntity<>(loc, HttpStatus.CREATED);
        } else {
            System.out.println("nema lokacije");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }
}