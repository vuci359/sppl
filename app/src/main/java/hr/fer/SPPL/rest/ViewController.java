package hr.fer.SPPL.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class ViewController {

    @GetMapping({ "/getData", "/postData" })
    public String index() {
        return "forward:/index.html";
    }
}