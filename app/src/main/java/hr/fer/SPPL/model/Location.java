package hr.fer.SPPL.model;

import com.sun.istack.NotNull;
import javax.persistence.*;
//import com.sun.jna.Native;



@Entity
@Table(name = "location")

// @UserDeserializer
// @JsonSerialize(using = UserSerializer.class)
public class Location {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "location_id", nullable=false)
    private long locationId;

    @NotNull
    @Column(name = "user_id", nullable=false)
    private int userId;

    //@NotNull
    @Column(name = "latitude", nullable=true)
    private double Latitude;

    //@NotNull
    @Column(name = "longitude", nullable=true)
    private double Longitude;

    //@NotNull
    @Column(name = "accuracy", nullable=true)
    private double Accuracy;

  //  @JacksonAnnotationsInside
  //  @JsonDeserialize(using = UserDeserializer.class)
    public Location(int user_id, double latitude, double longitude, double accuracy) {
        this.userId = user_id;
        this.Latitude = latitude;
        this.Longitude = longitude;
        this.Accuracy = accuracy;
    }

    public Location(){
        super();
    }

    public long getLocationId() {
        return locationId;
    }

    public int getUserId() {
        return userId;
    }

    public double getLatitude() {
       return Latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public double getAccuracy() {
        return Accuracy;
    }

    public void setLocationId(int location_id) {
        this.locationId = location_id;
    }

    public void setUserId(int user_id) {
        this.userId = user_id;
    }

    public void setLatitude(double latitude) {
        this.Latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.Longitude = longitude;
    }

    public void setAccuracy(double accuracy) {
        this.Accuracy = accuracy;
    }


    @Override
    public String toString() {
        return "{\"location\":{" +
                "\"latitude\":\"" + String.valueOf(Latitude) + '\"' +
                ", \"longitude\":\"" + String.valueOf(Longitude) + '\"' +
                "},"+
                "accuracy:"+String.valueOf(Accuracy)
                +"}";
    }
}
