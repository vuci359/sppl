package hr.fer.SPPL.repo;

import java.util.List;

import hr.fer.SPPL.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<Location, Long> {

    Location findByUserId(int userid);
    List<Location> findAll();

    Location findFirstByUserIdOrderByLocationIdDesc(int userid);

}
