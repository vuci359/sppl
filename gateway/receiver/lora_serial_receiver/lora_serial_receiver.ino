#include <SPI.h>
#include <LoRa.h>
#include "ESP8266WiFi.h"


#define ss 15
#define rst 16  // ne koristi se
#define dio0 5

void setup() {
  Serial.begin(115200);
  while (!Serial);

  Serial.println("LoRa Receiver");
  LoRa.setPins(ss, rst, dio0);    //setup LoRa transceiver module
  
  while (!LoRa.begin(866E6))     //433E6 - Asia, 866E6 - Europe, 915E6 - North America
  {
    Serial.println(".");
    delay(500);
  }
  LoRa.setSyncWord(0xCCA5);
  Serial.println("LoRa Initializing OK!");
}

void loop() {
  //Serial.println("nekaj bezveze");
  // try to parse packet
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    // received a packet
    Serial.println("Received packet:");

    // read packet
    while (LoRa.available()) {
     // Serial.println("podaci");
      Serial.print((char)LoRa.read());
    }
    // print RSSI of packet
    Serial.print("with RSSI ");
    Serial.println(LoRa.packetRssi());
    Serial.println("End of packet.");

  }
}
