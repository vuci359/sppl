#include "ESP8266WiFi.h"
#include <LoRa.h>
#include <SPI.h>
 
#define ss 15
#define rst 16  // ne koristi se
#define dio0 5

void setup()
{
  Serial.begin(115200);
  Serial.println();
  while (!Serial);
  Serial.println("LoRa Sender");
 
  LoRa.setPins(ss, rst, dio0);    //setup LoRa transceiver module
  
  while (!LoRa.begin(866E6))     //433E6 - Asia, 866E6 - Europe, 915E6 - North America
  {
    Serial.println(".");
    delay(500);
  }
  LoRa.setSyncWord(0xCCA5);
  Serial.println("LoRa Initializing OK!");

  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
}

void loop()
{
  Serial.print("Scan start ... ");
  int n = WiFi.scanNetworks();

  String wifi_data;
  wifi_data+="{";

  Serial.print(n);
  Serial.println(" network(s) found");  
  // wait until the radio is ready to send a packet
  while (LoRa.beginPacket() == 0) {
    Serial.print("waiting for radio ... ");
    delay(100);
  }
  LoRa.beginPacket();   //Send LoRa packet to receiver
  for (int i = 0; i < n; i++)
  {
    String MAC;
    uint8_t *bssid = WiFi.BSSID(i);
    for (byte i = 0; i < 6; i++){
      uint8_t pom=*bssid;
      if(pom<16) MAC=MAC+'0';
      MAC=MAC+String(pom,HEX);
      bssid++;
      if (i < 5){
        MAC=MAC+':';
        }
    }
    wifi_data=wifi_data+"\"macAddress\": ";
    wifi_data=wifi_data+'"'+MAC+"\", ";
    wifi_data=wifi_data+"\"signalStrength\": ";
    wifi_data=wifi_data+WiFi.RSSI(i)+", ";
    wifi_data=wifi_data+"\"channel\": ";
    wifi_data=wifi_data+WiFi.channel(i);
    wifi_data=wifi_data+'}';
    Serial.println(wifi_data);
    LoRa.println(wifi_data);
    wifi_data="{";

  }
  LoRa.endPacket(true);
  Serial.println("LoRa OK");
  delay(2500);
}
